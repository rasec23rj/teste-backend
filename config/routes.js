const express = require("express");
const auth = require("./auth");

const UserController = require("../controller/userController");
const apiController = require("../controller/apiController");
const ImbdapiController = require("../controller/imbdapiController");


module.exports = function (server) {
  
  /*
        Rotas fechadas
    */
  const router = express.Router();
  server.use("/api", router);
  router.use(auth);

  /**
   * Routes de usuario
   */

  server.use("/api", router);

  router.get("/imbdapi", apiController.list);
  router.post("/imbdapi", apiController.insert);
  router.get("/imbdapi/:id", apiController.listId);
  router.delete("/imbdapi/:id", apiController.deletar);

  router.put("/user/:id", UserController.update);
  router.delete("/user/:id", UserController.deletar);
  /**
   * Api do ombdapi.com
   */

  router.get("/ombdapi", ImbdapiController.list)
  /*
        Rotas abertas
    */
  const openApi = express.Router();
  server.use("/oapi", openApi);

  openApi.post("/login", UserController.login);
  openApi.post("/signup", UserController.signup);
  openApi.post("/valitateToken", UserController.validateToken);



};
