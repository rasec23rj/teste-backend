const express = require("express");
const server = express();
const allowCors = require("./cors");
const queryParser = require('express-query-int')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');
const swaggerJsDoc = require('swagger-jsdoc');

const shaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Teste Backend',
      description: 'Teste Backend information',
      servers: ["http://localhost:3003"]
    }
  },
  apis: ['routes.js']
}

const swaggerDocs = swaggerJsDoc(shaggerOptions)
server.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, swaggerDocs))

require("dotenv/config");
const port = process.env.PORT;
server.use(express.urlencoded({ extended: true }));
server.use(express.json());
server.use(allowCors);

server.use(queryParser());
server.listen(port, () => {
  console.log(`Server ${port}`);
});

module.exports = server;
