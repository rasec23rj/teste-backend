const ApiModel = require("../model/apiModel");
const {parseErrors} = require("../utils/error");

const list = async (req, res, next) => {
  try {
    const apiModel = await ApiModel.find({});
    res.send(apiModel);
  } catch (error) {
    res.send({ message: parseErrors(error) });
  }
};
const listId = async function (req, res) {
  try {
    const apiModel = await ApiModel.findById(req.params.id).exec();
    res.status(200).send(apiModel);
  } catch (error) {
    res.status(404).send({ message: parseErrors(error) });
  }
};
const buscar = async function (req, res, next) {
  try {
    const apiModel = await ApiModel.findById(req.params.id).exec();

    if (apiModel) {
      res.status(200).send(apiModel);
    } else {
      res.status(404).send({ message: parseErrors(error) });
    }
  } catch (error) {
    res.send({ message: parseErrors(error) });
  }
};
const insert = async function (req, res, next) {
  const apiModel = new ApiModel({
    imdbID: req.body.imdbID,
    imdbVotes: req.body.imdbVotes,
  });

  const apiModelFind = await ApiModel.findOne({imdbID: req.body.imdbID}).exec();
  
  try {

    if (req.body.imdbVotes >= 1 && req.body.imdbVotes <= 4) {
  
      if(apiModelFind) {
       var  imdbVotes = apiModelFind.imdbVotes + req.body.imdbVotes
       await ApiModel.updateOne(
          { imdbID: req.body.imdbID },
          {
            $set: {
              imdbVotes: imdbVotes,
            },
          }
        );
        //tt12801262
        //tt9140554
        
          res.status(200).send({ message: "Atualizadom com sucesso!" });
      }else{
        const save = apiModel.save();
        res.status(200).send({ message: save });
      }
      
    } else {
      res.status(400).send({ message: "o voto deve estar entre 1 e 4" });
    }
  
  } catch (error) {
    res.status(404).send({ message: parseErrors(error, res, next) });
  }
};
const update = async function (req, res, next) {
  const apiModel = new ApiModel({
    imdbVotes: req.body.imdbVotes,
  });

  const apiModelFind = await ApiModel.findOne({imdbID: req.params.id}).exec();
  try {

    if (req.body.imdbVotes >= 1 && req.body.imdbVotes <= 4) {
  
      if(apiModelFind) {
       var  imdbVotes = apiModelFind.imdbVotes + req.body.imdbVotes
       await ApiModel.updateOne(
          { imdbID: req.params.id },
          {
            $set: {
              imdbVotes,
            },
          }
        );
        //tt12801262
        //tt9140554
        
          res.status(200).send({ message: "Atualizadom com sucesso!" });
      }else{
        const save = apiModel.save();
        res.status(200).send({ message: save });
      }
      
    } else {
      res.status(400).send({ message: "o voto deve estar entre 1 e 4" });
    }
  
  } catch (error) {
    res.status(404).send({ message: parseErrors(error, res, next) });
  }
};
const deletar = async function (req, res, next) {
  const apiModel = await ApiModel.findById(req.params.id).exec();
  try {
    if (apiModel) {
      const ClientRemove = await ApiModel.deleteOne({ _id: req.params.id });
      res.status(200).send(ClientRemove);
    } else {
      res.status(404).send({ message: "Id não exister!" });
    }
    res.send(Client);
  } catch (error) {
    res.send({ message: parseErrors(error) });
  }
};

module.exports = { list, listId, buscar, insert, update, deletar };
