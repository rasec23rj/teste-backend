const errorsHandle = require("../utils/error");
const OmbdApi = require('../model/ombdapi')

const list = async (req, res, next) => {

  try {
    const ombdApi = await OmbdApi(req, res, next);
    console.log(req);

    res.send(ombdApi);
  
  } catch (error) {
    res.send({ message: errorsHandle(error) });
  }

};
module.exports = { list};
