const _ = require("lodash");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("../model/userModel");
const { parseErrors } = require("../utils/error");
const { emailRegex, passwordRegex } = require("../utils/regex");
require("dotenv").config();

const login = (req, res, next) => {
  const email = req.body.email || "";
  const password = req.body.password || "";

  User.findOne({ email }, (err, user) => {
    if (err) {
      return parseErrors(err, res);
    } else if (user && bcrypt.compareSync(password, user.password)) {
      const token = jwt.sign(user.toJSON(), process.env.authSecret, {
        expiresIn: "1 day",
      });
      const { nome, email } = user;
      res.json({ nome, email, token });
    } else {
      return res.status(400).send({ errors: ["Usuário/Senha inválidos"] });
    }
  });
};

const validateToken = (req, res, next) => {
  const token = req.body.token || "";

  jwt.verify(token, process.env.authSecret, function (err, decoded) {
    return res.status(200).send({ valid: !err });
  });
};

const signup = async (req, res, next) => {
  const nome = req.body.nome || "";
  const email = req.body.email || "";
  const perfil = req.body.perfil || "";
  const password = req.body.password || "";
  const confirmPassword = req.body.confirmPassword || "";
  const resultValidAdmin = await User.findOne({ _id: req.body.id }).exec();

  if (resultValidAdmin !== null) {
    if (resultValidAdmin.perfil !== "admin") {
      return res
        .status(403)
        .send({ errors: "Perfil de usuário não permitido" });
    }
  }

  if (!req.body.nome) {
    res.status(400).send({ errors: "nome é necessário." });
  }
  if (!req.body.password) {
    res.status(400).send({ errors: "password é necessário." });
  }
  if (!req.body.confirmPassword) {
    res.status(400).send({ errors: "confirmPassword é necessário." });
  }
  if (!emailRegex(email)) {
    return res.status(400).send({ errors: "O e-mail informado está inválido" });
  }

  if (!passwordRegex(password)) {
    return res.status(400).send({
      errors:
        "A senha precisar ter: pelo menos uma letra maiúscula, uma minúscula, um número e  " +
        "um caracter especial (@#$%) e tamanho entre 6 - 12. ",
    });
  }

  const salt = bcrypt.genSaltSync();
  const passwordHash = bcrypt.hashSync(password, salt);
  if (!bcrypt.compareSync(confirmPassword, passwordHash)) {
    return res.status(400).send({ errors: "Senhas não conferem." });
  }

  User.findOne({ email }, (err, user) => {
    if (err) {
      return parseErrors(err, res);
    } else if (user) {
      return res.status(400).send({ errors: "Usuário já cadastrado." });
    } else {
      const newUser = new User({ nome, email, perfil, password: passwordHash });
      newUser.save((err) => {
        if (err) {
          return parseErrors(err, res);
        } else {
          return res
            .status(201)
            .send({ message: "Usuário cadastrado com sucesso!." });
          //login(req, res, next);
        }
      });
    }
  });
};

const update = async function (req, res, next) {
  try {
    await User.updateOne(
      { _id: req.params.id },
      {
        $set: {
          nome: req.body.nome,
          email: req.body.email,
          perfil: req.body.perfil,
          password: req.body.password,
          confirmPassword: req.body.confirmPassword,
          status: req.body.status,
        },
      }
    );
    res.status(200).send({ message: "Atualizadom com sucesso!" });
  } catch (error) {
    res.send({ message: errorsHandle(error) });
  }
};
const deletar = async function (req, res, next) {
  var user = await User.findById(req.params.id).exec();
  try {
    if (user) {
      await User.updateOne(
        { _id: req.params.id },
        {
          $set: {
            status: "inativo",
          },
        }
      );
      res.status(200).send("Deletado com sucesso!");
    } else {
      res.status(404).send({ message: "Id não exister!" });
    }
    res.send(user);
  } catch (error) {
    res.send({ message: errorsHandle(error) });
  }
};

module.exports = { login, signup, validateToken, update, deletar };
