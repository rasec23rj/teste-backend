const restful = require("node-restful");
const mongoosePaginate = require('mongoose-paginate-v2');
const mongoose = restful.mongoose;
const ObjectId = mongoose.ObjectId;
const ApiSchema = new mongoose.Schema({
  id: ObjectId,
  imdbID: { type: String, required: true },
  imdbVotes: { type: Number, required: true },
  buff: Buffer,
}).plugin(mongoosePaginate);

module.exports = restful.model("Api", ApiSchema);
