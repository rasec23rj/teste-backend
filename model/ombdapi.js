const parseErrors = require("../utils/error");
const axios = require("axios");
const errorsHandle = require('../utils/error')
module.exports = async (req, res) => {
  const Url = `https://www.omdbapi.com/?${req.query.tipo}=${req.query.id}&apikey=1248c043`;
  try {
    var result;
    result = await axios.get(Url); 
    return result.data ;
  
  } catch (error) {
    const errors = parseErrors(error);
    res.status(500).json({ errors: errorsHandle(errors) });
  }
};
