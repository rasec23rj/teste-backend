const restful = require("node-restful");
const mongoose = restful.mongoose;
const ObjectId = mongoose.ObjectId;
const UserSchema = new mongoose.Schema({
  id: ObjectId,
  nome: { type: String, match: /[a-z]/, required: true },
  email: { type: String, required: true },
  perfil: { type: String, enum: ['admin', 'user'], required: true },
  password: { type: String, min: 6, max: 12, required: true },
  status: { type: String,  enum: ['ativo', 'inativo'], required: true, default: 'ativo' },
  buff: Buffer,
});

module.exports = restful.model("User", UserSchema);
