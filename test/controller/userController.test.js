describe("Testando rotas do client lista", () => {
  const axios = require("axios");
  const urlOapi = "http://localhost:3003/oapi/";
  const { makeid } = require("../util/utils");
  const User = require("../../model/userModel");
  /**
   * user "60e1112f294108d3ad541a6d"
   * admin "60e1a1cf294108d3ad541a71"
   * var result = await controller.update({ body: client.body, params: params });
   */
  test("Post signup rota - criar novo user sem nome deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        email: email,
        perfil: "admin",
        password: "Abcde23@#rj",
        confirmPassword: "Abcde23@#rj",
      },
    };

    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user sem password deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        confirmPassword: "Abcde23@#rj",
      },
    };

    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user sem confirmPassword deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        password: "Abcde23@#rj",
      },
    };

    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user error de  email sem @ inválido Regex deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        password: "Abcde23@#rj",
        confirmPassword: "Abcde23@#rj",
      },
    };
    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user error de  password sem Regex válido deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        password: "Abcde23rj",
        confirmPassword: "Abcde23rj",
      },
    };
    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user error de  password sem letra Maiscula inválido Regex deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        password: "abcde23@#rj",
        confirmPassword: "abcde23@#rj",
      },
    };
    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user error de  password sem letra Minuscula inválido Regex deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        password: "ABCDE23@#RJ",
        confirmPassword: "ABCDE23@#RJ",
      },
    };
    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user error de  password sem numeros inválido Regex deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        password: "Abcde@#rj",
        confirmPassword: "Abcde@#rj",
      },
    };
    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user error de  password comprimento de no minimo 6 deve retornar statusCode 400", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    const dados = {
      body: {
        nome: nome,
        email: email,
        perfil: "admin",
        password: "Abcd",
        confirmPassword: "Abcd",
      },
    };
    await axios.post(urlOapi + "signup", dados.body).catch((error) => {
      expect(error.response.status).toBe(400);
    });
  });
  test("Post signup rota - criar novo user admin deve retornar statusCode 201", async () => {
    await axios
      .post(urlOapi + "signup", {
        nome: "admin",
        email: "admin@email.com",
        perfil: "admin",
        password: "Abcde23@#rj",
        confirmPassword: "Abcde23@#rj",
      })
      .then((value) => {
        expect(value).toBe(201);
      })
      .catch((value) => {});
  });
  test("Post signup rota - criar novo user deve retornar statusCode 201", async () => {
    await axios
      .post(urlOapi + "signup", {
        nome: "user",
        email: "user@email.com",
        perfil: "user",
        password: "Abcde23@#rj",
        confirmPassword: "Abcde23@#rj",
        id: "60e4634516bc2f0781e67416",
      })
      .then((value) => {
        expect(value).toBe(201);
      })
      .catch((value) => {});
  });
  test("Post signup rota - criar novo user sem ser perfil de  admin deve retornar statusCode 403", async () => {
    var nome = makeid();
    var email = nome + "@email.com";
    dados = {
      nome: nome,
      email: email,
      perfil: "admin",
      password: "Abcde23@#rj",
      confirmPassword: "Abcde23@#rj",
      id: "60e4681fb6a05611f4b001df",
    };
    await axios.post(urlOapi + "signup", dados).catch((error) => {
      expect(error.response.status).toBe(403);
    });
  });
  test("Post signup rota - criar novo user como user, user já existente deve retornar statusCode 400", async () => {
    await axios
      .post(urlOapi + "signup", {
        nome: "user",
        email: "user@email.com",
        perfil: "user",
        password: "Abcde23@#rj",
        confirmPassword: "Abcde23@#rj",
        id: "60e4634516bc2f0781e67416",
      })

      .catch((value) => {
        expect(value.response.status).toBe(400);
      });
  });
  test("Post login rota - acessar com admin login deve retornar statusCode 200", async () => {
    await axios
      .post(urlOapi + "login", {
        email: "admin@email.com",
        password: "Abcde23@#rj",
      })
      .then((res) => {
        expect(res.status).toBe(200);
      });
  });
  test("Post login rota - acessar com user login deve retornar statusCode 200", async () => {
    await axios
      .post(urlOapi + "login", {
        email: "user@email.com",
        password: "Abcde23@#rj",
      })
      .then((res) => {
        expect(res.status).toBe(200);
      });
  });
});
